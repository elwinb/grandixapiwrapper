# Grandix API-wrapper #

The Grandix API-wrapper is an API-wrapper for using our API at https://www.grandix.nl/api/. You can use this API-wrapper in your own projects to communicate with out API.

Our main audience is Dutch, so we wrote the in-line comments in Dutch. In the next version of this wrapper we'll fix this.

### How do I get set up? ###

For using our API-wrapper you'll have to take the following steps:

* Sign up for our API at https://www.grandix.nl/api/sleutel/
* Download the API-wrapper
* Upload the files to your webserver
* Include the API-wrapper to your project and initiate the class

Read more about using the API-wrapper in the [Wiki](https://bitbucket.org/elwinb/grandixapiwrapper/wiki/).

### Documentation ###

The documentation for our API is located at https://www.grandix.nl/api/documentatie/. We currently only have Dutch documentation.