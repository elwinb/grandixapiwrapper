<?php 
/**
 * Grandix class.
 * 
 * Deze class is voor het gebruik van de API v2 van Grandix.nl. De class voert de
 * verzoeken richting de API uit en geeft de gevraagde data terug als bestand of string.
 * Raadpleeg het readme bestand voor gebruik van de class.
 *
 * Versie: 1.0
 * Datum: 2014-11-11
 */
class Grandix {
    /**
     * api
     *
     * Bevat de waarde van de API-sleutel. Nog geen API-sleutel? Vraag er een aan op
     * http://www.grandix.nl/api/.
     * 
     * (default value: null)
     * 
     * @var mixed
     * @access private
     */
    private $api = null;
    
    /**
     * cache
     *
     * Bevat de naam van de map waarin de cache bestanden komen te staan.
     * 
     * (default value: 'cache/')
     * 
     * @var string
     * @access private
     */
    private $cache = 'cache/';
    
    /**
     * url
     *
     * Bevat de URL van de API die aangeroepen wordt. Bij nieuwe versies van de API kan er
     * ook een nieuwe versie van deze class uitgebracht worden.
     * 
     * (default value: 'http://api.grandix.nl/2/')
     * 
     * @var string
     * @access private
     */
    private $url = 'http://api.grandix.nl/2/';
    
    /**
     * type
     *
     * Bevat een array met de type data dat opgevraagd kan worden. In de array staat ook
     * een waarde voor de maximale cache-leeftijd (in seconden) van een dataset. Om te
     * testen kan deze tijd verlaagd worden, maar in een live-omgeving heeft dit geen zin.
     * De data op de API wordt niet vaker vernieuwd dan aangegeven in deze array.
     * 
     * @var mixed
     * @access private
     */
    private $type = array(
        'image' => array(
            'current' => 300 ,
            'temperature' => 300,
            'forecast' => 300
        ),
        'data' => array(
            'current' => 300,
            'average' => 3600
        )
    );
    
    /**
     * format
     *
     * Bevat een array met de formats van tekst-bestanden die de class terug kan geven.
     * 
     * @var mixed
     * @access private
     */
    private $format = array(
        'data' => array('json','xml')
    );
    
    /**
     * __construct function.
     *
     * De constructor van de class controleert of er een API-sleutel is meegegeven. Als
     * deze is gevonden wordt er gecontroleert of de cache-map bestaat en beschrijfbaar
     * is.
     * Als aan een van deze voorwaarden niet is voldaan zal er een foutmelding gegenereerd
     * worden.
     * 
     * @access public
     * @param mixed $api
     * @return void
     */
    public function __construct($api) {
        // Controleer de API-sleutel
        if(!is_string($api) || strlen($api) != 32) {
            throw new Exception('API key not found.');
        }
        
        // Plaats de API-sleutel in een variabele
        $this->api = strtolower($api);
        
        // Controleer of er een cache-map is
        if(is_dir($this->cache) === false) {
            // Probeer de map aan te maken
            $handle = mkdir($this->cache);
            if($handle === false) {
                throw new Exception('The cache dir cannot be created. Please create dir in '.dirname(__FILE__).'/.');
            }
        }
        
        // Controleer of de cache-map beschrijfbaar is
        if(is_writable($this->cache) === false) {
            throw new Exception('The cache dir is not writable. Please check permissions for '.dirname(__FILE__).'/'.$this->cache.'.');
        }
    }
    
    /**
     * getImage function.
     *
     * Deze functie haalt de data op in de vorm van een afbeelding. Deze data wordt direct
     * als bestand (afbeelding) teruggegeven. Als de aanvraag niet correct is wordt er een
     * foutmelding gegenereerd.
     * 
     * @access public
     * @param string $type (default: 'current')
     * @return void
     */
    public function getImage($type = 'current') {
        // Controleer type
        if(!array_key_exists($type, $this->type['image'])) {
            throw new Exception('Image type not supported.');
        }
        
        // Bestandsnaam
        $filename = $this->cache.$type.'.png';
        
        // Controleer of de afbeelding in de cache staat
        $cache = $this->checkCache($filename);
        
        /**
            Als het bestand niet bestaat, of als de ouderdom groter is dan de maximale
            cache-leeftijd moet er een nieuwe afbeelding opgehaald worden.
        */
        if($cache === false || $cache > $this->type['image'][$type]) {
            $method = 'image/'.$type.'.png';
            $response = $this->getApiResponse($method,$filename);
        }
        
        // Lees het bestand nu uit de cache en geef het terug als afbeelding.
        header('Content-type: image/png');
        readfile($filename);
    }
    
    /**
     * getData function.
     *
     * Deze functie haalt de data op in de vorm van een JSON- of XML-bestand. De functie
     * geeft de data terug als ware het een bestand. Als er in de aanroep een fout zit
     * wordt er een foutmelding gegenereert.
     * 
     * @access public
     * @param string $type (default: 'current')
     * @param string $format (default: 'json')
     * @return void
     */
    public function getData($type = 'current', $format = 'json') {
        // Controleer type
        if(!array_key_exists($type, $this->type['data'])) {
            throw new Exception('Data type not supported.');
        }
        
        // Controleer format
        if(!in_array($format, $this->format['data'])) {
            throw new Exception('Data format not supported.');
        }
        
        // Bestandsnaam
        $filename = $this->cache.$type.'.'.$format;
        
        // Controleer of het bestand in de cache staat
        $cache = $this->checkCache($filename);
        
        /**
            Als het bestand niet bestaat, of als de ouderdom groter is dan de maximale
            cache-leeftijd moet er een nieuw bestand opgehaald worden.
        */
        if($cache === false || $cache > $this->type['data'][$type]) {
            $method = 'data/'.$type.'.'.$format;
            $response = $this->getApiResponse($method,$filename);
        }
        
        // Lees het bestand nu uit de cache en geef het terug in een bestand zoals opgevraagd.
        if($format == 'json') header('Content-type: application/json');
        else if($format == 'xml') header('Content-type: text/xml');
        readfile($filename);
    }
    
    /**
     * getDataString function.
     *
     * Deze functie is vergelijkbaar met getData(), alleen geeft deze de data niet terug
     * als bestand, maar als string.
     * 
     * @access public
     * @param string $type (default: 'current')
     * @param string $format (default: 'json')
     * @return void
     */
    public function getDataString($type = 'current', $format = 'json') {
        // Controleer type
        if(!array_key_exists($type, $this->type['data'])) {
            throw new Exception('Data type not supported.');
        }
        
        // Controleer format
        if(!in_array($format, $this->format['data'])) {
            throw new Exception('Data format not supported.');
        }
        
        // Bestandsnaam
        $filename = $this->cache.$type.'.'.$format;
        
        // Controleer of het bestand in de cache staat
        $cache = $this->checkCache($filename);
        
        /**
            Als het bestand niet bestaat, of als de ouderdom groter is dan de maximale
            cache-leeftijd moet er een nieuw bestand opgehaald worden.
        */
        if($cache === false || $cache > $this->type['data'][$type]) {
            $method = 'data/'.$type.'.'.$format;
            $response = $this->getApiResponse($method,$filename);
        }
        
        // Lees het bestand nu uit de cache en geef het terug als string.
        return file_get_contents($filename);
    }
    
    /**
     * getApiResponse function.
     *
     * Deze functie haalt de data io via de http-functie in de class. De data die binnen-
     * komt wordt in de cache opgeslagen.
     * Als de cache niet beschreven kan worden genereert de functie een foutmelding.
     * 
     * @access private
     * @param string $method (default: '')
     * @return void
     */
    private function getApiResponse($method = '', $filename = '') {
        if($method == '' || $filename == '') return false;
        
        // Gegevens ophalen
        $data = $this->http($method);

        // Schrijf de afbeelding weg op de server
        $write = file_put_contents($filename, $data['answer']);
        
        // Controleer of de afbeelding weggeschreven is
        if($write === false) {
            throw new Exception('Cannot write data in cache.');
        }
        
        return true;
    }
    
    /**
     * checkCache function.
     *
     * Deze functie controleert of een bestand in de cache aanwezig is. Als het bestand is
     * gevonden wordt de leeftijd van het bestand teruggegeven. Als het bestand niet is
     * gevonden geeft de functie false terug. Op basis van false of de cache-leeftijd kan
     * bepaald worden of het bestand opnieuw opgehaald meot worden.
     * 
     * @access private
     * @param string $filename (default: '')
     * @return void
     */
    private function checkCache($filename = '') {
        if($filename == '') return false;
        
        // Controleer of het bestand in de cache staat
        $is_file = is_file($filename);
        if($is_file === true) {
            // Leeftijd van het bestand uitrekenen
            $age = time() - filemtime($filename);
            
            // Geef leeftijd terug
            return $age;
        }
        
        return false;
    }
    
    /**
     * http function.
     *
     * Deze functie handelt de communicatie met de API-server af. De gegevens die worden
     * opgehaald worden in een array teruggegeven. Als er bij de communicatie met de
     * server, of bij het controleren van de binnengekomen data een fout optreedt zal de
     * functie de fout teruggeven.
     * 
     * @access private
     * @param string $method (default: '')
     * @return void
     */
    private function http($method = '') {
        if($method == '') return false;
        
        // Stel de volledige URL samen
        $url = $this->url.$method.'?key='.$this->api;
        
        // Maak cURL aan
        $ch = curl_init($url);
        
        // Verzend de aanvraag
        curl_setopt($ch, CURLOPT_HEADER,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        $answer = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);
        
        // Controleer de status-code
        if($info['http_code'] != 200) {
            throw new Exception($answer);
        }
        
        // Geef de data 1-op-1 terug
        return array(
            'info' => $info,
            'answer' => $answer
        );
    }
}
?>